use csv::ReaderBuilder;
use plotly::{
    color::{NamedColor, Rgb, Rgba},
    common::{
        ColorScale, ColorScalePalette, DashType, Fill, Font, Line, LineShape, Marker, Mode,
        Orientation, Title,
    },
    layout::{Axis, BarMode, Layout, Legend, TicksDirection, TraceOrder},
    //traces::table::{Cells, Header},
    Bar,
    Plot,
};
use std::collections::HashMap;
use std::env;
use std::fs;
use std::process;

#[derive(Eq, Hash, PartialEq, Debug)]
struct Config {
    eng: String,
    reg: String,
}

#[derive(Debug)]
struct Res {
    noop_data: Vec<i64>,
    noop_min: i64,
    noop_med: i64,
    run_data: Vec<i64>,
    run_min: i64,
    run_med: i64,
    setup_data: Vec<i64>,
    setup_min: i64,
    setup_med: i64,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = env::args().collect();

    let mut data = parse_data(&args[1]).unwrap_or_else(|err| {
        println!("Invalid arguments: {err}");
        process::exit(1);
    });

    let mut map: HashMap<Config, Res> = HashMap::new();
    for (key, mut val) in data.drain() {
        let key_vec = key.split(" ").collect::<Vec<&str>>();
        let eng = key_vec[1];
        let com = key_vec[2];
        let reg = key_vec[3];
        let conf = Config {
            eng: eng.to_owned(),
            reg: reg.to_owned(),
        };
        let maybe_res = map.get_mut(&conf);
        if let Some(res) = maybe_res {
            val.sort();
            match com {
                "run" => {
                    res.run_data = val.clone();
                    res.run_min = *val.first().unwrap();
                    res.run_med = val[val.len() / 2];
                }
                "setup" => {
                    res.setup_data = val.clone();
                    res.setup_min = *val.first().unwrap();
                    res.setup_med = val[val.len() / 2];
                }
                "noop" => {
                    res.noop_data = val.clone();
                    res.noop_min = *val.first().unwrap();
                    res.noop_med = val[val.len() / 2];
                }
                _ => {
                    println!("{com} didn't conform to expected <run/setup/noop>");
                    process::exit(1);
                }
            };
        } else {
            let mut res = Res {
                noop_data: Vec::new(),
                noop_min: 0,
                noop_med: 0,
                run_data: Vec::new(),
                run_min: 0,
                run_med: 0,
                setup_data: Vec::new(),
                setup_min: 0,
                setup_med: 0,
            };
            val.sort();
            match com {
                "run" => {
                    res.run_data = val.clone();
                    res.run_min = *val.first().unwrap();
                    res.run_med = val[val.len() / 2];
                }
                "setup" => {
                    res.setup_data = val.clone();
                    res.setup_min = *val.first().unwrap();
                    res.setup_med = val[val.len() / 2];
                }
                "noop" => {
                    res.noop_data = val.clone();
                    res.noop_min = *val.first().unwrap();
                    res.noop_med = val[val.len() / 2];
                }
                _ => {
                    println!("{com} didn't conform to expected <run/setup/noop>");
                    process::exit(1);
                }
            };
            map.insert(conf, res);
        }
    }
    /*for (key, val) in map.drain() {
            println!("{}-{}: NoOp: {}", key.eng, key.reg, val.noop_min);
            println!("       Setup: {}", val.setup_min - val.noop_min);
            println!("       Run: {}", val.run_min - val.setup_min);
    }*/
    //stacked_bar_chart(map);
    Ok(())
}

fn parse_data(
    data_folder: &String,
) -> Result<HashMap<String, Vec<i64>>, Box<dyn std::error::Error>> {
    let mut map: HashMap<String, Vec<i64>> = HashMap::new();
    for data_file in fs::read_dir(data_folder)? {
        let reader = ReaderBuilder::new()
            .has_headers(false)
            .from_path(data_file?.path())?;
        let mut record_iter = reader.into_records();

        while let Some(record) = record_iter.next() {
            let rec = record?;
            let maybe_vec = map.get_mut(&*rec.get(0).unwrap());
            if let Some(vec) = maybe_vec {
                vec.push(rec.get(1).unwrap().parse::<i64>().unwrap());
            } else {
                let vec: Vec<i64> = vec![rec.get(1).unwrap().parse::<i64>().unwrap()];
                map.insert(rec.get(0).unwrap().to_string(), vec);
            }
        }
    }
    return Ok(map);
}

fn stacked_bar_chart(mut map: HashMap<Config, Res>) {
    let mut commands = vec![];
    let mut run = vec![];
    let mut setup = vec![];
    let mut noop = vec![];
    for (key, val) in map.drain() {
        commands.push(format!("{}_{}", key.eng, key.reg));
        run.push(val.run_min - val.setup_min);
        setup.push(val.setup_min.clone() - val.noop_min);
        noop.push(val.noop_min.clone());
    }

    let trace1 = Bar::new(commands.clone(), run).name("Run");
    let trace2 = Bar::new(commands.clone(), setup).name("Setup");
    let trace3 = Bar::new(commands.clone(), noop).name("NoOp");

    let layout = Layout::new().bar_mode(BarMode::Stack);

    let mut plot = Plot::new();
    plot.add_trace(trace1);
    plot.add_trace(trace2);
    plot.add_trace(trace3);
    plot.set_layout(layout);

    plot.use_local_plotly();
    plot.write_html("out.html");
    plot.show();
}
